@extends('layout')
@section('titleText', 'Add New Item')
@section('content')
    <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Add New Item <div class="t-spinner fa-spin-2x pull-right"></div></h4>
                  <p class="card-category">Add New Property Item</p>
                </div>
                <div class="card-body">

                @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif

                @if(Session::has('itemCreated'))
                    <p class="text-success mt-3 mb-3 text-center">Your item has been added</p>
                @endif

                  <form id="add-item-form" action="{{route('createItem')}}" method="post" enctype="multipart/form-data">
                  <input type="hidden" value="{{Session::token()}}" name="_token">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Item Name</label>
                          <input name="name" value="{{old('name')}}" type="text" class="form-control">
                        </div>
                      </div>
                      
                      <div class="col-md-4">
                        <div class="form-group">
                          {{-- <label class="bmd-label-floating">Currency</label> --}}
                          <select class="form-control" name="currency"  value="{{old('currency')}}" >
                            <option value="GHS">GHS</option>
                            <option value="USD">USD</option>
                            <option value="GBP">GBP</option>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Price</label>
                          <input name="price"  value="{{old('price')}}"  type="text" class="form-control">
                        </div>
                      </div>

                    </div>

                    <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Location</label>
                          <input name="location" value="{{old('location')}}"  type="text" class="form-control">
                        </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Owner Name</label>
                          <input name="owner_name" value="{{old('owner_name')}}"  type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Owner Phone</label>
                          <input name="owner_phone" value="{{old('owner_phone')}}"  type="text" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <div class="form-group">
                            <label class="bmd-label-floating">Description</label>
                            <textarea name="description" class="form-control" rows="3">{{old('description')}}</textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                  <h4>Add Other Properties</h4>
                    <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                          <select class="form-control" name="property">
                            @foreach ($props as $prop)
                                <option value="{{$prop}}">{{$prop}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Property name </label>
                          <input name="property_name" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label class="bmd-label-floating">Property Value</label>
                          <input name="property_value" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <a id="add-property" href="javascript:void(0)" class="btn btn-info mt-4 mr-3">Add</a>
                        <a id="create-property" href="javascript:void(0)" class="btn btn-info mt-4">Create</a>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                          <div class="card">
                          {{-- <div class="card-header card-header-success">
                            <h4 class="card-title ">Messages</h4>
                            <p class="card-category"> People have questions/concerns</p>
                          </div> --}}
                          <div class="card-body">
                            <div class="table-responsive">
                              <table class="table">
                                <thead class=" text-primary">
                                  <tr>
                                  <th>Property name</th>
                                  <th>Property value</th>
                                  <th class="text-center">Remove</th>
                                </tr>
                                </thead>
                                <tbody id="prop_table" style="font-weight:500;">
                                      
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                          <label for="image1" class="btn btn-primary">Upload Image 1</label>
                          <label for="image2" class="btn btn-primary">Upload Image 2</label>
                          <label for="image3" class="btn btn-primary">Upload Image 3</label>
                          <label for="image4" class="btn btn-primary">Upload Image 4</label>
                          <label for="image5" class="btn btn-primary">Upload Image 5</label>

                          <input type="file" name="image1" id="image1" class="upload_img" style="display:none;">
                          <input type="file" name="image2" id="image2" class="upload_img" style="display:none;">
                          <input type="file" name="image3" id="image3" class="upload_img" style="display:none;">
                          <input type="file" name="image4" id="image4" class="upload_img" style="display:none;">
                          <input type="file" name="image5" id="image5" class="upload_img" style="display:none;">
                        </div>
                    </div>

                    {{-- <input type="hidden" name="_token" value="{{Session::token()}}"> --}}
                    <input type="hidden" name="properties" id="properties">
                    <button type="submit" class="btn btn-primary pull-right"><div class="t-spinner fa-spin-2x"></div>Create Item</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
@endsection
@section('script')
<script>
  function preview(keyVal) {
    var row = `<tr>
                  <td>${keyVal.name}</td>
                  <td>${keyVal.value}</td>
                  <td class="text-center">
                    <a href="javascript:void(0)" class="btn btn-danger btn-sm removeProp" title="Disable Admin">
                        <i class="material-icons">close</i>
                        <div class="ripple-container"></div>
                    </a>
                  </td>
              </tr>`
    $('#prop_table').append(row)
  }

  $(document).ready(function(event) {

    var keyValStore = []
    var images = []

    $(document).on('change', '.upload_img', function(event) {
      var el = $(this)
      var id = el.attr('id')
      var file = el.prop('files')[0]
      var label = $(`label[for="${id}"]`)
      if (file) {
        label.text(file.name)
        label.removeClass('btn-primary').addClass('btn-success')
      } else {
        label.text('UPLOAD IMAGE ' + id.split('e')[1])
        label.removeClass('btn-success').addClass('btn-primary')
      }
    })

    $(document).on('change', '#image', function(event){
      event.preventDefault()
      var files = $(this).prop('files')
      var content = ``
      for(var i = 0; i < files.length; i++) {
        images.push(files[i])
        content += `<span class="btn btn-danger removeImage" id="${files[i].name}" style="margin:3px;">${files[i].name} <i class="material-icons">cancel</i></span>`
      }
      $('#previews').html(content)
    })

    $(document).on('click', '.removeImage', function(event) {
      var el = $(this)
      var imageName = el.attr('id')
      for(var i = 0; i < images.length; i++) {
        if (images[i].name == imageName) {
          images.splice(i, 1);
        }
      }

      el.remove()
    })

    $(document).on('click', '.removeProp', function(event){
      var link = $(this)
      var prop = link.parent().parent().find('td:first-child').text()

      for(let i = 0; i < keyValStore.length; i++) {
        if (keyValStore[i].name.toLowerCase() == prop.toLowerCase()) {
          keyValStore.splice(i, 1)
        }
      }

      link.parent().parent().remove()

    })

    $(document).on('submit', '#add-item-form', function(event) {
      
      $('#properties').val(JSON.stringify(keyValStore))

    })

    $(document).on('click', '#create-property', function(event) {
      event.preventDefault()
      var prop_name = $('input[name="property_name"]').val()
      var prop_value = $('input[name="property_value"]').val()

      if (!prop_name || !prop_value) return false

      for(let i = 0; i < keyValStore.length; i++) {
        if (keyValStore[i].name.toLowerCase() == prop_name.toLowerCase()) {
          return
        }
      }

      keyValStore.push({name: prop_name, value: prop_value})
      preview({name: prop_name, value: prop_value})


      console.log(keyValStore)

    })
    
    $(document).on('click', '#add-property', function(event) {
      event.preventDefault()
      var prop_name = $('select[name="property"]').val()
      var prop_value = $('input[name="property_value"]').val()

      for(let i = 0; i < keyValStore.length; i++) {
        if (keyValStore[i].name.toLowerCase() == prop_name.toLowerCase()) {
          return
        }
      }

      keyValStore.push({name: prop_name, value: prop_value})
      preview({name: prop_name, value: prop_value})
    })
  })
</script>
@endsection