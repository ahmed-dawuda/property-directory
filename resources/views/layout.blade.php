<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    @yield('titleText')
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('assets/css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <!-- <link href="{{asset('assets/demo/demo.css')}}" rel="stylesheet" /> -->
  <style>
    .t-spinner{
      width: 20px;
      height: 20px;
      border-radius: 50%;
      border: 2px solid rgba(0,0,0, 0.5);
      border-top-color: white;
      /*display: inline-block;*/
      margin-bottom: -5px;
      margin-right: 10px;
      display: none;
    }

    .f-tag{
      padding: 10px;
      display: inline-block;
      {{-- background: green; --}}
    }

    .logo-icon {
      /*background: red;*/
      display: flex;
      justify-content:center;
    }

    #t, #b, #icon{
      width: 30px;
      height: 30px;
      display: flex;
      justify-content: center;
      align-items: center;
      font-weight: bolder;
      font-size: 20px;
      color: white;
    }

    #icon{
      background: #4CAF50;
      margin-right: 5px;
      height: 10px;
      width: 30px;
      border-radius: 50%;
      box-shadow: 0px 6px 0px #9C27B0, 0px 12px 0px #4CAF50, 0px 18px 0px #9C27B0, 0px 24px 0px #4CAF50;
      /*margin-top: 10px;*/
      /*border-bottom: 1px solid red;*/
    }

    .logo-icon #t{
      background: #4CAF50;
    }

    .logo-icon #b{
      background: white;
      color: #4CAF50;
      /*border: 5px solid #4caf50;*/
    }

    .fa-spin-2x {
      -webkit-animation: fa-spin 0.5s infinite linear;
      animation: fa-spin 0.5s infinite linear;
    }
  </style>
</head>

<body class="">
  <div class="wrapper ">
    <div class="sidebar" data-color="green" data-background-color="white" data-image="{{asset('assets/img/sidebar-1.jpg')}}">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo">
        <a href="javascript:void(0)" class="simple-text logo-normal">

          @if(!Auth::check())
            <div class="logo-icon">
              <div id="icon"></div>
              <div id="t">T</div>
              <div id="b">B</div>
            </div>
            @endif
          <div style="margin-top: 5px;">Terrabase</div>
          @if(Auth::check())
            {{--<hr>--}}
            @if(Auth::user()->image != 'default.png')
              <img src="{{asset('storage/'.Auth::user()->image)}}" style="width: 70px;height: 70px;border-radius: 50%;object-fit:cover;">
            @else
              <img src="{{asset('img/'.Auth::user()->image)}}" style="width: 70px;height: 70px;border-radius: 50%;object-fit:cover;">
            @endif
            <div style="margin-top: 5px;text-transform: capitalize;">{{Auth::user()->name}}</div>
          @endif
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">

        {{--@if(Auth::check())--}}
          {{--<li class="nav-item  {{url()->current() === route('dashboard') ? 'active' : ''}}">--}}
            {{--<a class="nav-link" href="{{route('dashboard')}}">--}}
              {{--<i class="material-icons">dashboard</i>--}}
              {{--<p>Dashboard</p>--}}
            {{--</a>--}}
          {{--</li>--}}
        {{--@endif--}}

          <li class="nav-item {{url()->current() === route('browse') || isset($item) ? 'active' : ''}}">
            <a class="nav-link" href="{{route('browse')}}">
              <i class="material-icons">vertical_split</i>
              <p>Browse</p>
            </a>
          </li>
          
          @if(!Auth::check())
          <li class="nav-item {{url()->current() === route('contact') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('contact')}}">
              <i class="material-icons">email</i>
              <p>Contact Us</p>
            </a>
          </li>

          <li class="nav-item {{url()->current() === route('signin') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('signin')}}">
              <i class="material-icons">lock_open</i>
              <p>Sign In</p>
            </a>
          </li>
          @endif
          {{-- <li class="nav-item {{url()->current() === route('signin') ? 'active' : ''}}" href="{{route('signin')}}">
            <a class="nav-link">
              <i class="material-icons">lock_open</i>
              <p>Login</p>
            </a>
          </li> --}}

         @if(Auth::check())
             
          

          @if(Auth::user()->register)
              <li class="nav-item {{url()->current() === route('register_admin') ? 'active' : ''}}">
                <a class="nav-link" href="{{route('register_admin')}}">
                  <i class="material-icons">create</i>
                  <p>Register</p>
                </a>
              </li>
            @endif

          @if(Auth::user()->messages)
          <li class="nav-item {{url()->current() === route('messages') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('messages')}}">
              <i class="material-icons">message</i>
              <p>Messages</p>
            </a>
          </li>
            @endif

          @if(Auth::user()->items)
          <li class="nav-item {{url()->current() === route('add_item') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('add_item')}}">
              <i class="material-icons">add_circle</i>
              <p>Add Item</p>
            </a>
          </li>
            @endif


          <li class="nav-item {{url()->current() === route('profile') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('profile')}}">
              <i class="material-icons">person_pin</i>
              <p>Profile</p>
            </a>
          </li>

          @if(Auth::user()->admins)
          <li class="nav-item {{url()->current() === route('admins') ? 'active' : ''}}">
            <a class="nav-link" href="{{route('admins')}}">
              <i class="material-icons">supervisor_account</i>
              <p>Admins</p>
            </a>
          </li>
            @endif


          <li class="nav-item ">
            <a class="nav-link" href="{{route('logout')}}">
              <i class="material-icons">power_settings_new</i>
              <p>Logout</p>
            </a>
          </li>
         @endif
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      @if(url()->current() === route('browse') || url()->current() == route('search'))
        @include('includes.nav')
      @endif
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <div class="container-fluid">
            @yield('content')
          </div>
        </div>
      </div>
      {{-- <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  Creative Tim
                </a>
              </li>
              <li>
                <a href="https://creative-tim.com/presentation">
                  About Us
                </a>
              </li>
              <li>
                <a href="http://blog.creative-tim.com">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://www.creative-tim.com/license">
                  Licenses
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
          </div>
        </div>
      </footer> --}}
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="../assets/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/popper.min.js" type="text/javascript"></script>
  <script src="../assets/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="../assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="{{asset('assets/js/plugins/chartist.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('assets/js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('assets/js/material-dashboard.min.js?v=2.1.0')}}" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <!-- <script src="{{asset('assets/demo/demo.js')}}"></script> -->
  @yield('script')
</body>

</html>