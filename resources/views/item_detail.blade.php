@extends('layout')
@section('titleText', $item->name.' '.$item->currency.' '.$item->price)
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-success">
                    <h4 class="card-title">{{$item->name}} </h4>
                    <p class="card-category" style="color: white;">{{$item->currency}} {{$item->price}}</p>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card-body">
                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner">
                                    @foreach($item['profile_images'] as $image)
                                        <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                                            <img class="d-block w-100" src="{{asset('storage/'.$image->name)}}" alt="{{$image->name}}">
                                        </div>
                                    @endforeach
                                </div>

                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title" style="font-weight: bold;">{{$item->name}} - {{$item->currency}} {{$item->price}}</h4>
                                <p style="margin-bottom: -10px;" class="mt-2">{{$item->description}}</p>
                            </div>
                            <div class="card-body table-responsive">
                                <table class="table table-hover">
                                    <tbody>
                                    <tr>
                                        <td><b>Location</b></td><td>{{$item->location}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Owner Name</b></td><td>{{$item->owner_name}}</td>
                                    </tr>
                                    <tr>
                                        <td><b>Owner Phone</b></td><td>{{$item->owner_phone}}</td>
                                    </tr>
                                    @foreach($item['properties'] as $property)
                                    <tr>
                                        <td><b>{{$property->name}}</b></td>
                                        <td>{{$property->value}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td><b>Posted On</b></td><td>{{$item->created_at}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            @if(Auth::check())
                                @if(Auth::user()->id == $item->admin_id || Auth::user()->id == 1)
                                    <a href="{{route('deleteItem', ['id'=> $item->id])}}" class="btn btn-danger pull-right" style="border-radius:0;">Delete</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection