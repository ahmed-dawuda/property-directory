@extends('layout')
@section('titleText', 'Contact Us')
@section('content')
    <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">CONTACT US</h4>
                  <p class="card-category">Send Us A Message</p>
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger mt-4">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if(Session::has('messageCreated'))
                    <p class="text-success text-center mb-2 mt-5"><b>Your message has been sent. You will hear from us shortly</b></p>
                @endif


                <div class="card-body">
                  <form method="post" action="{{route('createMessage')}}">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name</label>
                          <input name="name" value="{{old('name')}}" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone</label>
                          <input name="phone" value="{{old('phone')}}" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input name="email"  value="{{old('email')}}" type="email" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          {{-- <label>Message</label> --}}
                          <div class="form-group">
                            <label class="bmd-label-floating"> Enter your message</label>
                            <textarea  name="message" class="form-control" rows="5" >{{old('message')}}</textarea>
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" name="_token" value="{{Session::token()}}">
                    <button type="submit" class="btn btn-primary pull-right">Send Message</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
@endsection