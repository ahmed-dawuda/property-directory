@extends('layout')
@section('titleText', 'Register Admin')
@section('content')
    <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">UPLOAD IMAGES</h4>
                  <p class="card-category">Enter the details of the admin you want to register</p>
                </div>
                <div class="card-body">
                  <form method="post" action="{{route('registerAdmin')}}">
                  <input type="hidden" name="_token" value="{{Session::token()}}">

                  @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif

                   @if(Session::has('adminRegistered'))
                      <p class="text-success text-center mb-2 mt-5"><b>Registration successful, the credentials has been sent to the admin</b></p>
                  @endif

                    <div class="row mt-2">
                    @php
                        
                    @endphp

                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name</label>
                          <input name="name" value="{{old('name')}}" type="text" class="form-control">
                        </div>
                      </div>

                    </div>

                    
                    
                    <button type="submit" class="btn btn-primary pull-right">UPLOAD IMAGES</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
@endsection