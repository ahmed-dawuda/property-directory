@extends('layout')
@section('titleText', 'New Messages')
@section('content')
    <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title ">Messages</h4>
                  <p class="card-category"> People have questions/concerns</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <tr>
                        <th>Time</th>
                        <th>Name</th>
                        <th>Phone</th>
                        <th> Email</th>
                        <th class="text-center">Message</th>
                        <th class="text-center">Actions</th>
                      </tr>
                      </thead>
                      <tbody>

                        @foreach ($messages as $message)
                            <tr>
                          <td>{{$message->created_at}}</td>
                          <td>{{$message->name}}</td>
                          <td>{{$message->phone}}</td>
                          <td>{{$message->email}}</td>
                          <td class="text-primary" style="width:40%;">{{$message->message}}</td>
                          <td class="text-center">
                            <a href="{{route('replyMessage', ['message_id' => $message->id])}}" class="btn btn-primary btn-sm">
                                <i class="material-icons">done_all</i>
                                <div class="ripple-container"></div>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
@endsection