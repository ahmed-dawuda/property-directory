@extends('layout')
@section('titleText', 'Login')
@section('content')
    <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">SIGN IN</h4>
                  <p class="card-category">Enter the credentials</p>
                </div>
                <div class="card-body">
                  <form method="post" action="{{route('login')}}">
                    <div class="row">
                      <div class="col-md-12">

                      @if(Session::has('authFailed'))
                          <p class="text-danger text-center mb-2 mt-2">Sign In failed, invalid credentials</p>
                      @endif

                        <div class="form-group">
                          <label class="bmd-label-floating">Phone</label>
                          <input type="text" name="phone" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="password" name="password" class="form-control">
                          <input type="hidden" name="_token" value="{{csrf_token()}}">
                        </div>
                      </div>
                      
                    </div>
                    <button type="submit" class="btn btn-primary pull-right">SIGN IN</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
             <div class="col-md-4"></div>
          </div>
@endsection