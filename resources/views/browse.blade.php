@extends('layout')
@section('titleText', 'Browse Items')
@section('content')
    <div class="row">
      @foreach($items as $item)
        <div class="col-md-3">
          <div class="card card-chart">
            <div class="card-header card-header-success" style="padding:01px;border-radius:0;">
              @if(count($item['profile_images']))
              <img src="{{asset('storage/'.$item['profile_images'][0]->name)}}" style="width: 100%;height: 250px;object-fit: cover;">
              @endif
            </div>
            <div class="card-body">
              <h4 class="card-title">{{$item->name}}</h4>
              {{--<h4 class="card-title">{{$item->name}} <br> <b class="float-right">{{$item->currency}} {{$item->price}}</b></h4>--}}
              <p class="card-category">
                <span class="text-primary"><i class="material-icons">location_on</i></span> {{$item->location}} <br><br>
                <span class="text-primary"><i class="material-icons">access_time</i></span> Posted on {{$item->created_at}}
              </p>
            </div>
            <div class="card-footer">
              @if(Auth::check())
                @if(Auth::user()->id == $item->admin_id || Auth::user()->id == 1)
                  <a href="{{route('deleteItem', ['id'=> $item->id])}}" class="btn btn-danger btn-sm" style="border-radius:0;">Delete</a>
                @endif
              @endif
              <a href="{{route('detail', ['id'=> $item->id])}}" class="btn btn-warning btn-sm pull-right" style="border-radius:0;">More</a>
            </div>
          </div>
        </div>
@endforeach
    </div>
@endsection