@extends('layout')
@section('titleText', 'Edit Your Profile')
@section('content')
    <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Profile</h4>
                  <p class="card-category">Edit your profile</p>
                </div>
                <div class="card-body">
                  <form method="post" action="{{route('editProfile')}}" enctype="multipart/form-data">
                  <input type="hidden" name="_token" value="{{Session::token()}}">


                  @if ($errors->any())
                    <div class="alert alert-danger mt-2">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif

                @if(Session::has('unauthorizedUser'))
                    <p class="text-danger text-center mb-2 mt-5"><b>You're unauthorized to edit this account's profile</b></p>
                @endif

                @if(Session::has('profileUpdated'))
                    <p class="text-success text-center mb-2 mt-5"><b>Your Changes have been saved</b></p>
                @endif

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Name</label>
                          <input type="text" name="name" value="{{Auth::user()->name}}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Phone</label>
                          <input name="phone" value="{{Auth::user()->phone}}" type="text" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="text" name="email" value="{{Auth::user()->email}}" class="form-control">
                        </div>
                      </div>
                    </div>

                    <br>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Location</label>
                          <input type="text" name="location" value="{{Auth::user()->location}}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="bmd-label-floating">Occupation</label>
                          <input type="text" name="occupation" value="{{Auth::user()->occupation}}" class="form-control">
                        </div>
                      </div>
                      {{-- <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Email</label>
                          <input type="text" class="form-control">
                        </div>
                      </div> --}}
                    </div>

                    <br>

                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Old Password</label>
                          <input name="old_password" type="password" value="{{old('old_password')}}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">New Password</label>
                          <input name="password" type="password" value="{{old('password')}}" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="bmd-label-floating">Confirm New Password</label>
                          <input name="password_confirmation" value="{{old('password_confirmation')}}" type="password" class="form-control">
                        </div>
                      </div>
                    </div>

                      <label id="img-lb" for="image" class="btn btn-primary">UPLOAD PROFILE IMAGE</label>
                      <input type="file" name="image" id="image" style="display: none;">
                      <br><br>
                    
                    <button type="submit" class="btn btn-primary pull-right">SAVE CHANGES</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
@endsection
@section('script')
<script>
    $(document).ready(function (event) {
        $(document).on('change', '#image', function (event) {
            var image = $(this).prop('files')
            console.log(image)
//            console.log()
            if (image) {
                $('#img-lb').text(image[0].name).removeClass('btn-primary').addClass('btn-success')
//                console.log(image.name)
            } else {
                $('#img-lb').text('UPLOAD PROFILE IMAGE').removeClass('btn-success').addClass('btn-primary')
            }
        })
    })
</script>
@endsection