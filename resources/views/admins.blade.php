@extends('layout')
@section('titleText', 'Admins')
@section('content')
    <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title ">Messages</h4>
                  <p class="card-category"> People have questions/concerns</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table">
                      <thead class=" text-primary">
                        <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th> Email</th>
                        <th>Location</th>
                        <th>Occupation</th>

                            <th>Register?</th>
                            <th>Messages?</th>
                            <th>Create Items?</th>
                            <th>Admins?</th>
                            <th>Status</th>
                        <th class="text-center">Enable/Disable</th>
                      </tr>
                      </thead>
                      <tbody>

                        @foreach ($admins as $admin)
                            <tr>
                          <td>{{$admin->name}}</td>
                          <td>{{$admin->phone}}</td>
                          <td>{{$admin->email}}</td>
                          <td >{{$admin->location}}</td>
                          <td>{{$admin->occupation}}</td>


                                <td>
                                    @if(!$admin->register)
                                        <span class="badge badge-danger">No</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'register', 'value'=> true])}}" class="btn btn-success btn-sm" title="Enable Admin">
                                            <i class="material-icons">done_all</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @else
                                        <span class="badge badge-success">Yes</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'register', 'value'=> false])}}" class="btn btn-danger btn-sm" title="Disable Admin">
                                            <i class="material-icons">close</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if(!$admin->messages)
                                        <span class="badge badge-danger">No</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'messages', 'value'=> true])}}" class="btn btn-success btn-sm" title="Enable Admin">
                                            <i class="material-icons">done_all</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @else
                                        <span class="badge badge-success">Yes</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'messages', 'value'=> false])}}" class="btn btn-danger btn-sm" title="Disable Admin">
                                            <i class="material-icons">close</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if(!$admin->items)
                                        <span class="badge badge-danger">No</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'items', 'value'=> true])}}" class="btn btn-success btn-sm" title="Enable Admin">
                                            <i class="material-icons">done_all</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @else
                                        <span class="badge badge-success">Yes</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'items', 'value'=> false])}}" class="btn btn-danger btn-sm" title="Disable Admin">
                                            <i class="material-icons">close</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    @if(!$admin->admins)
                                        <span class="badge badge-danger">No</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'admins', 'value'=> true])}}" class="btn btn-success btn-sm" title="Enable Admin">
                                            <i class="material-icons">done_all</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @else
                                        <span class="badge badge-success">Yes</span>
                                        <a href="{{route('authorities', ['admin_id' => $admin->id, 'prop' => 'admins', 'value'=> false])}}" class="btn btn-danger btn-sm" title="Disable Admin">
                                            <i class="material-icons">close</i>
                                            <div class="ripple-container"></div>
                                        </a>
                                    @endif
                                </td>

                            <td><span class="badge badge-{{$admin->disabled ? 'danger' : 'success'}}">{{$admin->disabled ? 'Disabled' : 'Active'}}</span></td>
                            <td class="text-center">
                            @if($admin->disabled)
                                <a href="{{route('removeAdmin', ['admin_id' => $admin->id, 'type' => 'enable'])}}" class="btn btn-success btn-sm" title="Enable Admin">
                                    <i class="material-icons">done_all</i>
                                    <div class="ripple-container"></div>
                                </a>
                            @else
                                <a href="{{route('removeAdmin', ['admin_id' => $admin->id, 'type' => 'disable'])}}" class="btn btn-danger btn-sm" title="Disable Admin">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </a>
                            @endif
                          </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
@endsection