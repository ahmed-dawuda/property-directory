<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=> 'browse', 'uses'=> 'UserController@index']);
Route::get('/register', ['as'=> 'register_admin', 'uses'=> 'UserController@registerAdmin']);
Route::get('/dashboard', ['as'=> 'dashboard', 'uses'=> 'UserController@dashboard']);
Route::get('/add-item', ['as' => 'add_item', 'uses' => 'UserController@addItem']);
Route::get('/messages', ['as' => 'messages', 'uses' => 'UserController@messages']);
Route::get('/profile', ['as' => 'profile', 'uses'=> 'UserController@profile']);
Route::get('/login', ['as'=> 'signin', 'uses'=> 'UserController@login']);
Route::get('/contact-us', ['as'=> 'contact', 'uses'=> 'UserController@contactUs']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'UserController@logout']);
Route::get('/message/reply', ['as'=> 'replyMessage', 'uses' => 'UserController@replyMessage']);
Route::get('/admins', ['as'=> 'admins', 'uses' => 'UserController@getAdmins']);
Route::get('/admins/remove', ['as'=> 'removeAdmin', 'uses' => 'UserController@removeAdmin']);
Route::get('/admins/authorities', ['as'=> 'authorities', 'uses'=> 'UserController@authorities']);
Route::get('/upload/{item_id}/{count}', ['as'=> 'uploads', 'uses' => 'UserController@uploadImage']);
Route::get('/item/{id}', ['as'=> 'detail', 'uses'=> 'UserController@itemDetail']);
Route::get('/delete-item/{id}', ['as'=> 'deleteItem', 'uses'=> 'UserController@deleteItem']);
Route::get("/search", ['as'=> 'search', 'uses'=> 'UserController@search']);

Route::post('/login', ['as'=> 'login', 'uses'=> 'UserController@loginToAccount']);
Route::post('/messages', ['as'=> 'createMessage', 'uses'=> 'UserController@createMessage']);
Route::post('/profile', ['as' => 'editProfile', 'uses' => 'UserController@editUserProfile']);
Route::post('/register', ['as' => 'registerAdmin', 'uses'=> 'UserController@createAdmin']);
Route::post('/item', ['as'=> 'createItem', 'uses'=> 'UserController@createItem']);
Route::post('/images', ['as' => 'saveImages', 'uses'=> 'UserController@saveImages']);