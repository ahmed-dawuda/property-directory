<?php

namespace App\Http\Controllers;

use App\ProfileImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Message;
use App\Item;
use App\Property;
use Illuminate\Support\Facades\Storage;
use Validator;

class UserController extends Controller
{
    public function index() 
    {
        $items = Item::with('properties', 'profile_images')->orderBy('created_at', 'DESC')->get();
//        return $items;
        return view('browse')->with(['items'=> $items]);
    }

    public function registerAdmin() 
    {
        return view('register');
    }

    public function dashboard() 
    {
        return view('dashboard');
    }

    public function addItem() 
    {
        $props = Property::distinct('name')->pluck('name');
        return view('add_item')->with(['props'=> $props]);
    }

    public function messages() 
    {
        $messages = Message::where('admin_id', null)->orderBy('created_at', 'DESC')->get();
        return view('messages')->with(['messages'=> $messages]);
    }

    public function profile() 
    {
        return view('profile');
    }

    public function login()
    {
        return view('login');
    }

    public function contactUs() 
    {
        return view('contact_us');
    }

    public function logout() 
    {
        Auth::logout();
        return redirect()->route('browse');
    }

    public function loginToAccount(Request $request)
    {

        $credentials = $request->only('phone', 'password');

        if (Auth::attempt($credentials)) {
            if (!Auth::user()->disabled) {
                return redirect()->route('browse');
            } 
        }

        return redirect()->back()->with('authFailed', 'Invalid Credentials');
    }

    public function createMessage(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'phone' => 'required|numeric|min:10',
            'email' => 'required|email',
            'message' => 'required'
        ]);

        Message::create($validatedData);
        
        return redirect()->back()->with('messageCreated', 'Your message has been sent');
    }

    public function editUserProfile(Request $request)
    {
        $rules = [
            'name'=> 'required',
            'phone'=> 'required|numeric|min:10',
            'occupation' => 'required',
            'location' => 'required',
            'email' => 'required',
            'old_password'=> 'required'
        ];

        if ($request->password) {
            $rules['password'] = 'required|confirmed|min:6';
        }

        $validatedData = $request->validate($rules);

        $user = Auth::user();

        if (!Hash::check($request->old_password, $user->password)) {
            return redirect()->back()->with('unauthorizedUser', 'you are unauthorized');
        }

        
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->occupation = $request->occupation;
        $user->location = $request->location;
        $user->email = $request->email;
        
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $filename = random_int(1111, 99999).time() . '.' . $image->getClientOriginalExtension();
            Storage::disk('local')->put('public/'.$filename, file_get_contents($image));
            $user->image = $filename;
        }

        $user->save();

        return redirect()->back()->with('profileUpdated', 'changes saved');
    }

    public function createAdmin(Request $request)
    {
        $validatedData = $request->validate([
            'name'=> 'required',
            'phone'=> 'required|numeric|min:10|unique:users',
            'occupation' => 'required',
            'location' => 'required',
            'email' => 'required|unique:users',
        ]);
        
        $passphrase = random_int(1111111, 9999999);
        $validatedData['password'] = bcrypt($passphrase);
        $validatedData['image'] = 'default.png';

    
        $phone = $request->phone;
        $message = 'Your credentials are Phone: '.$phone.' and Password: '.$passphrase;

         $uri = 'https://apps.mnotify.net/smsapi?key='.env('SMS_KEY').'&to='.$phone.'&msg='.$message.'&sender_id=Lamdsa';

         $client = new Client();
         $res = $client->request('GET', $uri);
        
        User::create($validatedData);

        return redirect()->back()->with('adminRegistered', 'admin created');
    }

    public function replyMessage(Request $request)
    {
        $message = Message::find($request->message_id);
        $message->admin_id = Auth::user()->id;
        $message->save();

        return redirect()->route('messages');
    }

    public function getAdmins(Request $request)
    {
        $admins = User::where('id', '<>', 1)->get();
        return view('admins')->with(['admins'=> $admins]);
    }

    public function removeAdmin(Request $request)
    {
        $admin = User::find($request->admin_id);
        if ($admin) {
            if ($request->type == 'disable') {
                $admin->disabled = true;
                $admin->register = false;
                $admin->items = false;
                $admin->messages = false;
                $admin->admins = false;
            } else {
                $admin->disabled = false;
            }
            $admin->save();
        }

        return redirect()->route('admins');
    }

    public function authorities(Request $request)
    {
        $admin = User::find($request->admin_id);

//        return $admin;

//        return $request;

        switch ($request->prop) {
            case 'items':
                $admin->items = (boolean) $request->value;
                break;
            case 'register':
                $admin->register = (boolean) $request->value;
                break;
            case 'messages':
                $admin->messages = (boolean) $request->value;
                break;
            case 'admins':
                $admin->admins = (boolean) $request->value;
                break;
        }

//        return $admin;

        $admin->save();
        return redirect()->route('admins');
    }

    public function createItem(Request $request)
    {

        $data = $request->validate([
            'name'=> 'required',
            'location' => 'required',
            'currency' => 'required',
            'owner_name' => 'required',
            'owner_phone' => 'required|numeric|min:10',
            'price'=> 'required|regex:/^\d*(\.\d{1,2})?$/',
            'properties' => 'required',
            'description' => 'required'
        ]);

        $validatedData = [
            'name' => $request->name,
            'location' => $request->location,
            'currency' => $request->currency,
            'owner_name' => $request->owner_name,
            'owner_phone' => $request->owner_phone,
            'description' => $request->description,
            'price' => $request->price,
            'admin_id' => Auth::user()->id
        ];

        $item = Item::create($validatedData);

        $properties = json_decode($request->properties);
        $props = [];
        foreach($properties as $property) {
            $props[] = [
                'name' => $property->name,
                'value' => $property->value,
                'item_id' => $item->id
            ];
        }

        Property::insert($props);

        $imgs = ['image1', 'image2', 'image3', 'image4', 'image5'];
        $names = [];

//        return $request->file('image1');
        
        foreach($imgs as $img) {

            if ($request->hasFile($img)) {
                $image = $request->file($img);
//                return $image;
                $filename = random_int(1111, 99999).time() . '.' . $image->getClientOriginalExtension();

                Storage::disk('local')->put('public/'.$filename, file_get_contents($image));
                $names[] = [
                    'name' => $filename,
                    'item_id' => $item->id
                ];
            }
        }

        ProfileImage::insert($names);

        return redirect()->back()->with('itemCreated', 'you item is created');
    }

    public function uploadImage(Request $request, $item_id, $count)
    {
        return view('upload_images')->with(['count'=> $count, 'item_id'=> $item_id]);
    }

    public function saveImages(Request $request)
    {
        return $request;
    }

    public function itemDetail(Request $request, $id)
    {
        $item = Item::with('properties', 'profile_images')->find($id);
        return view('item_detail')->with(['item'=> $item]);
    }

    public function deleteItem(Request $request, $id)
    {
        Item::find($id)->delete();
        return redirect()->route('browse');
    }

    public function search(Request $request)
    {
        $terms = $request->term;

//        return $request;

        if(!$terms) {
            return redirect()->route('browse');
        }
        
        $terms = explode(' ', $terms);

//        return $terms;

        $items = Item::with('properties', 'profile_images');
        
        foreach ($terms as $term) {
            $items->where('location', 'LIKE', '%'.$term.'%')
                ->orWhere('name', 'LIKE', '%'.$term.'%')
                ->orWhere('price', 'LIKE', '%'.$term.'%');
        }

        return view('browse')->with(['items'=> $items->get()]);
    }
}

