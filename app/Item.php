<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $guarded = ['id'];

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function profile_images()
    {
        return $this->hasMany(ProfileImage::class);
    }
}
