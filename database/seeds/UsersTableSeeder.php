<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'John',
            'email' => 'tabijohn17@gmail.com',
            'phone' => '0279270112',
            'occupation'=> 'Student',
            'register'=> true,
            'items'=> true,
            'messages' => true,
            'admins' => true,
            'image'=> 'default.png',
            'location' => 'KNUST',
            'password' => bcrypt('secret')
        ]);
    }
}
